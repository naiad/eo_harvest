from datetime import datetime

import shapely.geometry

import eo_harvest


res = eo_harvest.search_granule(
    'Ifremer',
    'argo',
    datetime(2019, 1, 1), datetime(2019, 1, 12),
    shapely.geometry.box(-100, 0, 1.53, 43.68),
)

for c in res.items:
    print(c.name, c.url, c.quicklook)

res.items[0].download()

