from datetime import datetime

import shapely.geometry

import eo_harvest

#cat = eo_harvest.catalogue('PODAACDrive')
#for c in cat.items:
#    print(c)

res = eo_harvest.search_granule(
    'PODAACDrive', 'PODAAC-ASOP2-12C01',
     datetime(2019, 9, 1), datetime(2019, 9, 2),
     shapely.geometry.box(-20, -20, 20, 20)
)

for c in res.items:
    print(c)
