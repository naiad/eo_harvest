from datetime import datetime

import shapely.geometry

import eo_harvest

def test_thredds_type():
    res = eo_harvest.search_granule(
        'Wekeo',
        "EO:MO:DAT:OCEANCOLOUR_GLO_CHL_L3_REP_OBSERVATIONS_009_065:dataset-oc-glo-chl-multi_cci-l3-chl_4km_daily-rep-v02",
        datetime(1997, 9, 4), datetime(1997, 9, 5),
        area=shapely.geometry.box(-50, -20, -20, 20),
        items_per_page=10,
        silent=False
    )
    assert(len(res.items) == 1)
    for c in res.items:
        print(c)

def test_oc1():
    res = eo_harvest.search_granule(
        'Wekeo',
       # "EO:MO:DAT:OCEANCOLOUR_GLO_CHL_L3_REP_OBSERVATIONS_009_065:dataset-oc-glo-chl-multi_cci-l3-chl_4km_daily-rep-v02",
        "EO:MO:DAT:OCEANCOLOUR_ATL_CHL_L3_NRT_OBSERVATIONS_009_036:dataset-oc-atl-chl-multi_cci-l3-chl_1km_daily-rt-v02",
        datetime(2017, 6, 4), datetime(2017, 6, 5),
        area=shapely.geometry.box(-50, -20, -20, 20),
        items_per_page=10,
        silent=False
    )
    for c in res.items:
        print(c)

def test_sst_med():
    res = eo_harvest.search_granule(
        'Wekeo',
        "EO:MO:DAT:SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012:SST_MED_SST_L3S_NRT_OBSERVATIONS_010_012_a",
        datetime(2017, 6, 4), datetime(2017, 6, 5),
        area=shapely.geometry.box(-50, -20, -20, 20),
        items_per_page=10,
        silent=False
    )
    for c in res.items:
        print(c)

#res = test_thredds_type()
res = test_sst_med()


#res.items[0].download()

