from datetime import datetime

import shapely.geometry

import eo_harvest

#cat = eo_harvest.catalogue('PODAACDrive')
#for c in cat.items:
#    print(c)

res = eo_harvest.search_granule(
    'Wekeo',
   # 'EO:ESA:DAT:SENTINEL-1:SAR',
    'EO:ESA:DAT:SENTINEL-2:MSI',
    datetime(2020, 5, 31),
    datetime(2020, 6, 1),
    shapely.geometry.box(-1, 65.5, 11., 68.),
    items_per_page=5
)

for c in res.items:
    print(c)

print("Download")
res.items[0].download()
