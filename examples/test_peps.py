from datetime import datetime

import shapely.geometry

import eo_harvest


# res = eo_harvest.search_granule(
#     'PEPS',
#     'S1',
#     datetime(2020, 6, 1), datetime(2020, 6, 6),
#     shapely.geometry.box(-1, 58., 7., 62.),
#     #product_type='GRD'
# )

res = eo_harvest.search_granule(
    'PEPS',
    'S1',
    datetime(2020, 1, 1), datetime(2020, 1, 12),
    shapely.geometry.box(1.13, 43.9, 1.53, 43.68),
    #product_type='GRD'
)

for c in res.items:
    print(c.name, c.url, c.quicklook)
