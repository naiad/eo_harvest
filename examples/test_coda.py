from datetime import datetime

import shapely.geometry

import eo_harvest
import eo_harvest.utils

#area = shapely.geometry.box(-50, -20, -20, 20)
area = shapely.geometry.box(-180, 70, 180, 90)
#area = shapely.geometry.box(-180, -90, 180, 90)

res = eo_harvest.search_granule(
    'CODA',
#    'SL_2_WST___',
    'SL_1_RBT___',

    datetime(2020, 1, 1), datetime(2020, 1, 5),
    area,
   # platform="S3A",
    fetchall=True
)
print("TOTAL ", len(res.items))
#eo_harvest.utils.plot_search_result(res, clip=area)

print("Download")
#res.items[0].download()
print("end download")

for c in res.items:
    print(c.name)
