from datetime import datetime

import shapely.geometry

import eo_harvest

# define search area as shapely geometry (here a simple rectangular box)
area = shapely.geometry.box(-50, -20, -20, 20)

# define search time frame
start = datetime(2021, 1, 1)
end = datetime(2021, 1, 10)

# performs the search
# first args is the data store, here `Ifremer`
res = eo_harvest.search_granule(
    'Ifremer',
    'ascat_b_l2b_12km_knmi',
    start,
    end,
    area,
)
print(res.items)
