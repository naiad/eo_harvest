# encoding: utf-8
"""
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from setuptools import setup, find_packages


__package_name__ = 'eo-harvest'
__version__ = '0.1.0dev'


setup(
    name=__package_name__,
    version=__version__,
    author="Jeff Piolle (Ifremer)",
    author_email="jean.francois.piolle@ifremer.fr",
    description=(
        "a python API to search EO data files (granules) from various data"
        "stores"
    ),
    license="LICENSE.txt",
    keywords="ocean observation data search",
    url="https://gitlab.ifremer.fr/naiad/eo-harvest.git",
    packages=find_packages(),
    install_requires=[
        'cartopy',
        'lxml',
        'bs4',
        'podaacpy',
        'pyyaml'
    ],
    entry_points={
        'eo_harvest.stores': [
            'PODAACDrive = eo_harvest.stores.podaacdrive:PODAACDrive',
            'Naiad = eo_harvest.stores.naiad:Naiad',
            'Wekeo = eo_harvest.stores.wekeo:Wekeo',
            'PEPS = eo_harvest.stores.peps:PEPS',
            'CODA = eo_harvest.stores.coda:CODA',
            'Ifremer = eo_harvest.stores.ifremer:Ifremer',
        ]
    },
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)
