from collections import OrderedDict

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
from shapely.geometry import LineString, Point, MultiLineString

from eo_harvest.queries.granule import Granule


def plot_search_result(result, clip=None):

    # map background
    fig = plt.figure(figsize=(15, 15))

    ax = fig.add_subplot(projection=ccrs.PlateCarree())
    land_50m = cfeature.NaturalEarthFeature(
        'physical', 'land', '50m', edgecolor='face',
        facecolor=cfeature.COLORS['land'])
    ax.add_feature(land_50m)
    ax.coastlines('50m')
    gl = ax.gridlines(draw_labels=True)
    gl.top_labels = False
    gl.left_labels = False

    def __get_shape(granule):
        if granule.shape is not None:
            return granule.shape
        elif granule.boundaries is not None:
            return granule.boundaries

    # add footprints
    footprints = []
    if isinstance(result.items, OrderedDict):
        # multi-search
        for time, granules in result.items.items():
            for g in granules:
                if __get_shape(g):
                    footprints.append(__get_shape(g))
    else:
        for g in result.items:
            if __get_shape(g):
                footprints.append(__get_shape(g))
    if len(footprints) > 0:
        if isinstance(footprints[0], Point):
            x, y = zip(*[(_.x, _.y) for _ in footprints])
            ax.scatter(x, y, color='g', alpha=0.9, edgecolor='k')

        elif isinstance(footprints[0], (LineString, MultiLineString)):
            from shapely import plotting
            for f in footprints:
                plotting.plot_line(f, ax=ax, color='r', alpha=0.9,
                                   add_points=False)

        else:
            ax.add_geometries(footprints, crs=ccrs.PlateCarree(), color='g',
                              alpha=0.5, facecolor='g', edgecolor='k')
    if clip is not None:
        ax.add_geometries([clip], crs=ccrs.PlateCarree(), color='b', alpha=0.2)

    plt.show()
