class Response(object):
    def __init__(
            self, items,
            total_results=None,
            start_page=None,
            items_per_page=None):
        self.items = items
        self.total_results = total_results
        self.start_page = start_page
        self.items_per_page = items_per_page

    def next(self):
        # return self.start_index + self.items_per_page
        return self.start_page + 1

    def complete(self) -> bool:
        return (self.start_page + 1) * self.items_per_page >= self.total_results

    def save(self, outfile):
        files = []
        if isinstance(self.items, list):
            for g in self.items:
                files.append(g.url)
        else:
            for _, items in self.items:
                for g in items:
                    files.append(g.url)
        files = list(set(files))
        files.sort()
        with open(outfile, 'w') as outf:
            for f in files:
                outf.write(f+'\n')
