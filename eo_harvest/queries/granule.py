#from eo_harvest import _stores

class Granule(object):
    def __init__(self, name, url, store, dataset_id,
                 quicklook=None,
                 start_time=None,
                 end_time=None,
                 boundaries=None,
                 shape=None,
                 opendap_url=None):
        self.name = name
        self.url = url
        self.dataset_id = dataset_id
        self.quicklook = quicklook
        self.start = start_time
        self.end = end_time
        self.opendap_url = opendap_url
        self.boundaries = boundaries
        self.shape = shape

        self._store = store

    def __str__(self):
        gstr = '{}\n'.format(self.name)
        if self.start is not None:
            gstr += '   start: {}\n'.format(self.start)
        if self.end is not None:
            gstr += '   end: {}\n'.format(self.end)
        if self.boundaries is not None:
            gstr += '   bounding box: {}\n'.format(self.boundaries)
        if self.shape is not None:
            gstr += '   footprint: {}\n'.format(self.shape)
        gstr += '   url: {}\n'.format(self.url)
        if self.opendap_url is not None:
            gstr += '   opendap: {}\n'.format(self.opendap_url)
        if self.quicklook is not None:
            gstr += '   quicklook: {}\n'.format(self.quicklook)
        gstr += '\n'
        return gstr

    def display_quicklook(self, **kwargs):
        return self._store.display_quicklook(self)

    def download(self, **kwargs):
        return self._store.download(self, **kwargs)
