class Dataset(object):
    def __init__(self, ident, title):
        self.id = ident
        self.title = title

    def __str__(self):
        return '[{}] {}'.format(self.id, self.title)
