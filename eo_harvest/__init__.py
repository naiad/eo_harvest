import os
from pathlib import Path
from typing import Union, List, Tuple

import pkg_resources
import yaml

from .utils import plot_search_result

_stores = {
        entry_point.name: entry_point
        for entry_point
        in pkg_resources.iter_entry_points('eo_harvest.stores')
}


def get_store_from_config(store: str, config: str = None):
    """Get the store classname to be used

    Args:
        store (str): name of the store, which should be an entry in the
            configuration file.
        config (str, optional): configuration file containing the definition
            of a store. If not provided, the will search for
            NAIAD_HOME env variable or by default in the default
            ``.naiad/stores.yaml`` file in the user home directory.
    """
    if config is None:
        if 'NAIAD_HOME' in os.environ:
            config = Path(os.environ['NAIAD_HOME']) / 'stores.yaml'
        else:
            # look in user home
            config = Path.home() / '.naiad/stores.yaml'

    if not config.exists():
        raise IOError(
            'stores configuration file {} not found'.format(config)
        )

    with open(config) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    if store not in config['stores']:
        raise ValueError(
            'store {} missing in configuration file'.format(store))

    entry = config['stores'].pop(store)
    if 'type' in entry:
        return entry['type']

    return entry


def get_store(store: str, **kwargs):
    try:
        return _stores[store].load()(**kwargs)
    except KeyError:
        # search in config file
        base_store = get_store_from_config(store)
        return get_store(base_store, name=store, **kwargs)


def search_granule(store: str, dataset, start_time, end_time, area, **kwargs):
    return get_store(store, **kwargs).search_granule(
        dataset, start_time, end_time, area, **kwargs)


def download(store: str, granule, mystore='.', **kwargs):
    return get_store(store, **kwargs).download(
        granule, mystore=mystore, **kwargs)


def catalogue(store: str, **kwargs):
    return get_store(store, **kwargs).catalogue()


def multisearch(
        store: str,
        dataset: str,
        targets: List[Tuple['datetime.datetime', 'shapely.geometry']],
        delta_time: Union[
            'datetime.timedelta',
            Tuple['datetime.timedelta', 'datetime.timedelta']
        ],
        filter_duplicates=True,
        **kwargs):
    return get_store(store, **kwargs).multisearch(
        dataset, targets, delta_time, filter_duplicates=filter_duplicates,
        **kwargs)


def plot(result, clip=None):
    plot_search_result(result, clip=clip)
