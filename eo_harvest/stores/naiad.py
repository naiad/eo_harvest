import naiad

from .store import Store
from eo_harvest.queries.dataset import Dataset
from eo_harvest.queries.response import Response


class Naiad(Store):
    store = 'Naiad'

    def search_granule(
            self,
            dataset, start_time, end_time, area,
            fetch_all=True,
            start_page=0,
            items_per_page=400,
            **kwargs):

        complete = False
        granules = []
        while not complete:
            resp = naiad.search(
                dataset=dataset,
                start_time=start_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
                end_time=end_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
                area='{},{},{},{}'.format(*area.bounds),
                start_index=str(start_page),
                items_per_page=str(items_per_page),
                _format='atom'
            )

            granules = resp

            response = Response(granules, rtotal, rstart, rcount)
            if fetch_all and not response.complete():
                start_page = response.next()
            else:
                complete = True

        return response

    def catalogue(
            self,
            fetch_all=True,
            start_index=0,
            items_per_page=50,
            **kwargs):

        datasets = []

        for dataset in self.config.keys():
            datasets.append(
                    Dataset(dataset, '')
                )

        return Response(datasets, len(datasets), 0, len(datasets))

