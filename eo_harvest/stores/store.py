from collections import OrderedDict
import logging
import os
from pathlib import Path
import requests
#from requests.adapters import TimeoutHTTPAdapter
#from requests.packages.urllib3.util.retry import Retry

import subprocess
from typing import Union, List, Tuple

from PIL import Image
import matplotlib.pyplot as plt
import shapely
import yaml

from eo_harvest.queries.response import Response


class Store(object):
    """
    Args:
        config: path to the configuration file with store connection details
        name: name of the store in configuration file. If different stores share
            the same access API (store class), a name must be provided to get
            the related configuration details of a specific store instance.
    """
    store = 'base'
    max_items_per_page = 100

    def __init__(self, config: str = None, name: str = None, **kwargs):
        # by default the name of the store is the same as its class type.
        if name is None:
            self.name = self.store
        else:
            self.name = name

        self.config = self.get_config_from_file(config)

        # allow retries and timeout handling
        # retry_strategy = Retry(
        #     total=3,
        #     backoff_factor=1,
        #     status_forcelist=[429, 500, 502, 503, 504],
        #     method_whitelist=["HEAD", "GET", "OPTIONS"]
        # )
        # adapter = TimeoutHTTPAdapter(max_retries=retry_strategy)
        # self.http = requests.Session()
        # self.http.mount("https://", adapter)
        # self.http.mount("http://", adapter)

    def search_granule(self, dataset, start_time, end_time, area, **kwargs):
        raise NotImplementedError

    def get_local_storage_path(self, granule, mystore: 'Path'):
        return (Path(mystore) / Path(granule._store.name)
                / Path(granule.dataset_id)
                / Path(granule.start.strftime('%Y/%j'))
                / granule.name)

    def download(self, granule, mystore='.', protocol='FTP',
                 with_wget=True, silent=True, **kwargs):
        # get result
        outpath = self.get_local_storage_path(granule, mystore)
        if not outpath.exists():
            outpath.parent.mkdir(parents=True, exist_ok=True)

        if silent is False:
            print("Downloading: {} to {}".format(granule.url, outpath))

        if with_wget:
            return self._wget_download(
                granule, outpath, protocol=protocol, **kwargs)
        else:
            return self._simple_download(granule, outpath, **kwargs)

    @classmethod
    def _wget_download(cls, granule, outpath, protocol='FTP',
                       username=None, password=None, **kwargs):
        args = []
        for k, v in kwargs.items():
            args.extend([k, v])

        if username is not None and password is not None:
            args.extend(["--user", username, "--password", password])
        elif 'username' in granule._store.config:
            args.extend(["--user", granule._store.config['username'],
                         "--password", granule._store.config['password']])

        if isinstance(granule.url, str):
            url = granule.url
        elif isinstance(granule.url, dict):
            if protocol not in granule.url:
                raise ValueError(
                    'Protocol {} is not permitted for this granule. Allowed '
                    'protocols are: {}'.format(protocol, granule.url.keys()))
            url = granule.url[protocol]
        else:
            raise TypeError("Wrong content for URL")

        command = [
            "wget", "-c", "--auth-no-challenge",
            "--timeout", '7200',
            '--output-document', str(outpath),
            *args, str(url)]
        try:
            download_state = subprocess.call(command)
        except Exception as e:
            print("Failed execution of: {}".format(
                ' '.join(command)))
            raise

        if download_state != 0:
            print("Failed (status={}) execution of: {}".format(
                download_state, ' '.join(command)))

        # if download_state==0 => successful download
        return download_state == 0

    @classmethod
    def _simple_download(cls, granule, outpath, **kwargs):
        handle = open(outpath, 'wb')
        with requests.get(granule.url, allow_redirects=True, stream=True) as r:
            for chunk in r.iter_content(chunk_size=1024 * 1024):
                if chunk:  # filter out keep-alive new chunks
                    handle.write(chunk)
        return True

    def display_quicklook(self, granule):
        if granule.quicklook is None:
            raise ValueError("Not available for this granule.")

        auth = None
        if 'username' in self.config:
            auth = (self.config['username'], self.config['password'])

        response = requests.get(
                granule.quicklook,
                auth=auth,
                stream=True)
        img = Image.open(response.raw)

        plt.imshow(img)
        plt.show()
        return

    def catalogue(
            self,
            fetch_all=True,
            start_page=0,
            items_per_page=50,
            **kwargs):
        raise NotImplementedError

    def get_config_from_file(self, config: str = None):
        """Get the parameters for a specific store.

        Args:
            config (str, optional): configuration file containing the definition
                of a store. If not provided, the will search for
                NAIAD_HOME env variable or by default in the default
                ``.naiad/stores.yaml`` file in the user home directory.
        """
        if config is None:
            if 'NAIAD_HOME' in os.environ:
                config = Path(os.environ['NAIAD_HOME']) / 'stores.yaml'
            else:
                # look in user home
                config = Path.home() / '.naiad/stores.yaml'

        if not config.exists():
            raise IOError(
                'Index configuration file {} not found'.format(config)
            )

        with open(config) as f:
            config = yaml.load(f, Loader=yaml.FullLoader)

        if self.name not in config['stores']:
            raise ValueError(
                'index {} missing in configuration file'.format(self.name))

        return config['stores'][self.name]

    def decode_search_result(self, resp):
        # abstract method
        raise NotImplementedError

    def _decode_granule(self, entry, dataset_id, **kwargs):
        # abstract method
        raise NotImplementedError

    def search_granule(
            self,
            dataset, start_time, end_time, area,
            fetch_all=True,
            start_page=0,
            items_per_page=0,
            silent=True,
            **kwargs):

        if items_per_page == 0:
            items_per_page = self.max_items_per_page
        elif items_per_page > self.max_items_per_page:
            logging.warning(
                "Can't return more than {} items at once"
                .format(self.max_items_per_page)
            )
            items_per_page = self.max_items_per_page

        complete = False
        granules = []
        while not complete:
            logging.info("Getting result page {}".format(start_page))
            resp = self._search(
                dataset_id=dataset,
                start_time=start_time,
                end_time=end_time,
                area=area,
                start_page=start_page,
                items_per_page=items_per_page,
                silent=silent,
                **kwargs
            )

            entries, rtotal, rstart, rcount = self.decode_search_result(resp)

            granules.extend(
                [self._decode_granule(_, dataset) for _ in entries])

            response = Response(granules, rtotal, rstart, items_per_page)
            if fetch_all and not response.complete():
                start_page = response.next()

            else:
                complete = True

        return response

    @staticmethod
    def search_area_by_radius(target, radius):
        return target.buffer(radius)

    def multisearch(
            self,
            product: str,
            targets: List[Tuple['datetime.datetime', 'shapely.geometry']],
            delta_time: Union[
                'datetime.timedelta',
                Tuple['datetime.timedelta', 'datetime.timedelta']
            ],
            filter_duplicates: bool = True,
            prefetch_all: bool = False,
            radius: Union[None, int] = None,
            clipping_radius: Union[None, int] = None,
            silent=True,
            **kwargs):
        """Search for granules over a sequence of spatio-temporal targets.

        Can be used for instance to search over hurricane track locations/

        Args:
            product (str): product to search for
            targets: list of tuples (date, area) to search over
            delta_time: time criteria to apply for each search date
            filter_duplicates: if True, remove duplicates and keep the closest
                match in time within the provided search dates
            prefetch_all: if True, first query over the whole search area (union
                of the targets) then post-filter this result to remove
                non-crossovers (based on time and window). This allows to
                minimize the number of queries to the remote store (may be
                faster in many cases), down side is that more results are
                 transfered, most of them dummy.
            radius: spatial colocation radius in degrees, if the targets are
                Point geometries
            clipping_radius: radius for clipping the result geometries, in
                degrees. Only applicable when `radius` is set.
            """
        searches = []
        s_targets = targets

        # time window
        if isinstance(delta_time, tuple):
            dt_before, dt_after = delta_time
        else:
            dt_before = dt_after = delta_time

        # search radius
        if radius is not None:
            # only applicable to targets provided as plot.
            all_points = all([isinstance(_[1], shapely.geometry.Point)
                              for _ in targets])
            if not all_points:
                raise ValueError(
                    "targets must be provided as Point geometries to use search"
                    " search radius")

            search_radius = radius
            if clipping_radius is not None and clipping_radius > radius:
                search_radius = clipping_radius

            s_targets = [
                (t, Store.search_area_by_radius(s, search_radius))
                for t, s in targets]

        crossovers = OrderedDict([])
        if not prefetch_all:
            for t, area in s_targets:
                searches.append(
                    self.search_granule(
                        product, area=area,
                        start_time=t-dt_before, end_time=t+dt_after,
                        fetch_all=True,
                        silent=silent,
                        **kwargs)
                )

            if len(searches) > 0:
                for i, target in enumerate(s_targets):
                    crossovers[target[0]] = searches[i].items

        else:
            area = shapely.ops.unary_union(
                [target for _, target in s_targets])
            start = s_targets[0][0] - dt_before
            end = s_targets[-1][0] + dt_after
            granules = self.search_granule(
                    product, area=area,
                    start_time=start, end_time=end,
                    fetch_all=True,
                    silent=silent,
                    **kwargs).items

            # filter out crossovers not matching in time
            for target in s_targets:
                crossovers[target[0]] = []

            if len(granules) > 0:
                # order chronologically
                granules.sort(key=lambda x: x.start)

                t0 = 0
                for granule in granules:
                    while t0 < len(s_targets) and \
                            s_targets[t0][0] + dt_after < granule.start:
                        t0 += 1

                    t = t0
                    # check the next granules are really crossovers
                    while ((t < len(s_targets)) and
                           (granule.start >= s_targets[t][0] - dt_before) and
                           (granule.start <= s_targets[t][0] + dt_after) and
                           s_targets[t][1].intersects(granule.shape)):
                        granule.shape = s_targets[t][1].intersection(
                            granule.shape)
                        crossovers[s_targets[t][0]].append(granule)
                        t += 1

        # filter out results not in colocation radius
        if clipping_radius is not None:
            crossovers_in_radius = OrderedDict([])
            for i, crossover in enumerate(crossovers.items()):
                t, granules = crossover
                sel_granules = [
                    _ for _ in granules if _.shape.intersects(
                        Store.search_area_by_radius(targets[i][1], radius))]
                crossovers_in_radius[t] = sel_granules

            crossovers = crossovers_in_radius

        # filter out duplicates : keep closest to target times
        if filter_duplicates:
            duplicates = {}
            # collect duplicated granules
            for t, crossover in crossovers.items():
                for g in crossover:
                    if g.name not in duplicates:
                        duplicates[g.name] = []
                    duplicates[g.name].append((g, t))

            # remove duplicates
            granules = []
            for g in duplicates:
                if len(duplicates[g]) == 1:
                    # granule only found once
                    granules.append(duplicates[g][0])
                else:
                    # select closest in time
                    selection = 0
                    item, t = duplicates[g][selection]
                    min_delta = abs((item.start - t).total_seconds())
                    for i, (item, t) in enumerate(duplicates[g][1:]):
                        delta = abs((item.start - t).total_seconds())
                        if delta < min_delta:
                            selection = 1
                            min_delta = delta
                    granules.append(duplicates[g][selection])

            crossovers = OrderedDict([])
            for i, target in enumerate(s_targets):
                crossovers[target[0]] = []
            for g, t in granules:
                crossovers[t].append(g)

        response = Response(crossovers, len(crossovers), 0, len(crossovers))

        return response

