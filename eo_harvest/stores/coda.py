"""
EUMETSAT CODA data store
"""
import logging
import requests
from urllib.parse import urljoin, quote

from dateutil import parser
import matplotlib.pyplot as plt
from PIL import Image
import shapely.wkt
import shapely.ops

from eo_harvest.queries.granule import Granule
from eo_harvest.stores.opensearch import Opensearch


class CODA(Opensearch):
    store = 'CODA'
    max_items_per_page = 100

    def __init__(self, **kwargs):
        super(CODA, self).__init__(**kwargs)

    def catalogue(
            self,
            fetch_all=True,
            start_page=0,
            items_per_page=50,
            **kwargs):
        raise NotImplementedError

    def _decode_granule(self, entry, dataset_id, **kwargs):
        import re
        download = entry.find(
            href=re.compile("Products"))
        quicklook = entry.find(
            rel=re.compile("icon"))
        footprint = entry.find_all(attrs={"name": "footprint"})[0]

        return Granule(
            name=entry.title.text,
            url=download.attrs['href'],
            store=self,
            dataset_id=entry.find_all(attrs={"name": "producttype"})[0].text,
            quicklook=quicklook.attrs['href'],
            shape=shapely.wkt.loads(footprint.text),
            start_time=parser.parse(
                entry.find('date', attrs={"name": "beginposition"}).text
            ).replace(tzinfo=None),
            end_time=parser.parse(
                entry.find('date', attrs={"name": "endposition"}).text
            ).replace(tzinfo=None),
        )

    def decode_search_result(self, resp):
        entries, rtotal, rstart, rcount = super(
            CODA, self).decode_search_result(resp)

        return entries, rtotal, max(0, rstart // rcount), rcount,

    def _search(
            self,
            dataset_id, start_time, end_time,
            area=None,
            fetch_all: bool = True,
            start_page: int = 0,
            items_per_page: int = 100,
            platform: str = None,
            instrument=None,
            timeliness='Non Time Critical',
            other_keywords=None,
            silent=True,
            **kwargs
            ):
        """
        Args:
            platform (str): a value among: "S3A", "S3B"
        """
        url = urljoin(
            self.config['url'],
            '/search?q='
        )
        url += ('(beginPosition:[{}Z TO {}Z] OR endPosition:[{}Z TO {}Z])'
                .format(start_time.isoformat(), end_time.isoformat(),
                        start_time.isoformat(), end_time.isoformat()))
        keywords = []
        if platform is not None:
            # there is no keyword to select the mission in CODA!
            # we have to use a filter on the product filename
            keywords.append('filename:{}*'.format(platform))
        if instrument is not None:
            keywords.append('instrumentshortname:{}'.format(instrument))
        if dataset_id is not None:
            keywords.append('producttype:{}'.format(dataset_id))
        if timeliness is not None:
            keywords.append('timeliness:{}'.format(timeliness))
        if area is not None:
            sarea = shapely.ops.split(
                area, shapely.geometry.LineString(
                    ([(0, -90), (0, 90)])
                ))
            keywords.append('({})'.format(
                ' OR '.join([
                    'footprint:"Intersects({})"'.format(
                        shapely.geometry.polygon.orient(geo).wkt)
                    for geo in sarea])
            ))

        if other_keywords is not None:
            for k, v in other_keywords.items():
                keywords.append('{}:{}'.format(k, v))
        if len(keywords) > 0:
            url += ' AND ({})'.format(' AND '.join(keywords))

        url += '&start={}&rows={}'.format(
            start_page * items_per_page, items_per_page)

        logging.info('fetching {}'.format(quote(url, safe='/:?&=')))
        if not silent:
            print('fetching {}'.format(quote(url, safe='/:?&=')))
        resp = requests.get(
            quote(url, safe='/:?&='),
            auth=(self.config['username'], self.config['password'])
        )

        if resp.status_code != requests.codes.ok:
            raise ConnectionError('search request failed')
        #print(resp.text)
        return resp.text

    def display_quicklook(self, granule):
        response = requests.get(
                granule.quicklook,
                auth=(self.config['username'], self.config['password']),
                stream=True)
        img = Image.open(response.raw)

        plt.imshow(img)
        plt.show()
        return

    # def download(self, granules, mystore='.', **kwargs):
    #     if isinstance(granules, Granule):
    #         granules = [granules]
    #     for granule in granules:
    #         outpath = self.get_local_storage_path(granule, mystore)
    #         if not outpath.exists():
    #             outpath.parent.mkdir(parents=True, exist_ok=True)
    #         with requests.get(
    #             granule.url, allow_redirects=True, stream=True,
    #             auth=(self.config['username'], self.config['password'])
    #         ) as r:
    #             with open(outpath, 'wb') as f:
    #                 shutil.copyfileobj(r.raw, f)
    #
