import base64
import copy
import json
import logging
import requests
import time
from urllib.parse import urljoin, quote

from dateutil import parser

from .store import Store
from eo_harvest.queries.granule import Granule


class Wekeo(Store):
    store = 'Wekeo'
    max_items_per_page = 10

    def __init__(self, **kwargs):
        super(Wekeo, self).__init__(**kwargs)
        self._token = None
        self._license = None

    def _get_token(self):
        if self._token is not None:
            return self._token

        credentials = base64.urlsafe_b64encode(
            '{}:{}'.format(
                self.config['username'],
                self.config['password']
            ).encode('ascii')
        ).decode('ascii')

        r = requests.get(
            urljoin(self.config['url'], 'gettoken'),
            headers={'authorization': 'Basic {}'.format(credentials)}
        )

        if r.status_code != requests.codes.ok:
            raise ConnectionError('gettoken request failed')
        self._token = r.json()['access_token']
        #print("TOKEN ", self._token)
        return self._token

    def _delete_job(self, job_id):
        r = requests.delete(
            urljoin(self.config['url'], 'datarequest/job {}'.format(job_id)),
            headers={
                'content-type': 'application/json',
                'authorization': '{}'.format(self._get_token())
            }
        )
        if r.status_code != requests.codes.ok:
            raise ConnectionError('delete job request failed')

    def _get_running_jobs(self):
        r = requests.get(
            urljoin(self.config['url'], 'datarequest/jobs?status=running'),
            headers={'authorization': '{}'.format(self._get_token())}
        )
        if r.status_code != requests.codes.ok:
            raise ConnectionError('jobs request failed')
        return r.json()['content']

    def decode_search_result(self, resp):
        entries = resp['content']
        if len(entries) == 0:
            return [], 0, 0, 0,
        rtotal = int(resp['totItems'])
        rstart = int(resp['page'])
        rcount = int(resp['itemsInPage'])

        return entries, rtotal, rstart, rcount,

    def get_query_metadata(self, dataset_id):
        """returns the metadata giving the query terms and values for a dataset

        fails for some datasets (error 500) such as Sentinel ones.
        """
        r = requests.get(
            urljoin(self.config['url'], 'querymetadata/{}'.format(dataset_id)),
            headers={'authorization': self._get_token(),
                     'content-type': 'application/json'},
        )
        if r.status_code == 505:
            # not available for Sentinel products
            return
        return r.json()

    def _get_default_term(self, term):
        if 'details' not in term:
            raise NotImplementedError("Missing term details in {term}")

        details = term['details']
        if 'groupedValueLabels' in details:
            # select all possible values by default
            return list([
                _['valuesLabels'].values()
                for _ in details['groupedValueLabels']][0])

        elif 'valuesLabels' in details:
            choices = details['valuesLabels']
            selected = list(choices.values())[0]
            if len(choices) >= 1:
                logging.info("Multiple choices available for {term['name'}")
                logging.info("  selected: {selected}")
            return selected

        raise NotImplementedError("Can not handle {}".format(term))

    def _add_default_required(self, params, data):
        """Add the missing mandatory terms defined in the query metadata"""
        for category in params:
            if category in ['boundingBoxes', 'dateRangeSelects']:
                continue
            if params[category] is None:
                logging.debug('skipping {}'.format(category))
                continue

            catvalues = category[:-1] + 'Values'

            for item in params[category]:
                if item['isRequired']:
                    if catvalues not in data:
                        data[catvalues] = []

                    found = [
                        _ for _ in data[catvalues] if _['name'] == item['name']]
                    if len(found) == 0:
                        # not yet in the query. Fill in with default values
                        data[catvalues].append(
                            {'name': item['name'],
                             'value': self._get_default_term(item)}
                        )

        return data

    def _add_date_as_stringchoice(self, params, data, start, end):
        terms = params["stringChoices"]
        if "stringChoiceValues" not in data:
            data["stringChoiceValues"] = []
        has_year = len([_ for _ in terms if _['name'] == 'year']) == 1
        if has_year:
            data["stringChoiceValues"].append(
                {'name': 'year', 'value': str(start.year)})

        has_day = len([_ for _ in terms if _['name'] == 'day']) == 1
        if has_day:
            data["stringChoiceValues"].append(
                {'name': 'month', 'value': "{:02d}".format(start.month)})
            data["stringChoiceValues"].append(
                {'name': 'day', 'value':  "{:02d}".format(start.day)})
            return data

        has_month = len([_ for _ in terms if _['name'] == 'month']) == 1
        if has_month:
            data["stringChoiceValues"].append(
                {'name': 'month', 'value':  "{:02d}".format(start.month)})
            return data

        raise ValueError(
            "Expected date selection keywords in {}".format(terms))

    def _format_query(self,
                      dataset_id, start_time, end_time, area,
                      product_type=None):
        api = self.get_query_metadata(dataset_id)
        #print(json.dumps(api, indent=4,  sort_keys=True))
        if api is None:
            logging.debug("No available query metadata: using default query for"
                          "Sentinel data")
        else:
            logging.debug("Query metadata: {}".format(
                json.dumps(api, indent=4,  sort_keys=True)))
        params = api['parameters']

        data = {"datasetId": dataset_id}

        # time selection
        if api is None:
            data["dateRangeSelectValues"] = [{
                "name": "dtrange",
                "start": start_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "end": end_time.strftime("%Y-%m-%dT%H:%M:%SZ")}]

        elif ("dateRangeSelects" in params
                and params["dateRangeSelects"] is not None):
            data["dateRangeSelectValues"] = [{
                "name": params["dateRangeSelects"][0]["name"],
                "start": start_time.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "end": end_time.strftime("%Y-%m-%dT%H:%M:%SZ")}]

        elif "stringChoices" in params and params["dateRangeSelects"] is None:
            data = self._add_date_as_stringchoice(
                params, data, start_time, end_time)

        else:
            raise NotImplementedError

        if area is not None and params['boundingBoxes'] is not None:
            data["boundingBoxValues"] = [
                {"name": "bbox", "bbox": list(area.envelope.bounds)}
            ]

        if product_type is not None:
            data["stringChoiceValues"] = [
                {"name": "producttype", "value": product_type}]

        data = self._add_default_required(params, data)
        logging.debug("Query : {}".format(
            json.dumps(data, indent=4,  sort_keys=True)))
        return data

    def _search(self, dataset_id, start_time, end_time, area,
                product_type=None,
                start_page=0,
                items_per_page=100,
                silent=True,
                ):
        if self._get_license() is None:
            self._get_license()

        # check if there are pendind requests
        jobs = self._get_running_jobs()
        if len(jobs) != 0:
            logging.warning("There are running jobs. Cancelling them")
            for job_id in jobs:
                self._delete_job(job_id)

        data = self._format_query(
            dataset_id, start_time, end_time, area,
            product_type=product_type)

        params = {'size': items_per_page, 'page': start_page}

        url = urljoin(self.config['url'], 'datarequest')

        logging.info('fetching {} with {}'.format(
            quote(url, safe='/:?&='), data))
        if not silent:
            print('fetching {} with {}'.format(
                quote(url, safe='/:?&='), data))

        # first page only
        if start_page == 0:
            r = requests.post(
                urljoin(self.config['url'], 'datarequest'),
                data=json.dumps(data),
                headers={'authorization': self._get_token(),
                         'content-type': 'application/json'},
                params=params
            )
            if r.status_code != requests.codes.ok:
                raise ConnectionError(
                    'datarequest request failed : {}'.format(r.text))

            # job is in queue
            self.job = r.json()['jobId']
            while r.json()['status'] in ['started', 'running']:
                r = requests.get(
                    urljoin(self.config['url'],
                            'datarequest/status/{}'.format(self.job)),
                    headers={'authorization': self._get_token()},
                )
                time.sleep(1)

            job_status = r.json()
            if job_status['status'] == 'failed':
                logging.error('The request failed with the following message {}'
                              .format(job_status['message']))

        # get result
        r = requests.get(
            urljoin(self.config['url'],
                    'datarequest/jobs/{}/result'.format(self.job)),
            headers={'authorization': self._get_token()},
            params=params

        )
        #print(urljoin(self.config['url'],
        #            'datarequest/jobs/{}/result'.format(self.job)))
        #print(params)
        #print(r.json())
        return r.json()

    def _decode_granule(self, entry, dataset_id, **kwargs):
        return Granule(
            name=entry['filename'],
            url=entry['url'],
            store=self,
            dataset_id=entry['productInfo']['datasetId'],
            start_time=parser.parse(
                entry['productInfo']['productStartDate']
            ).replace(tzinfo=None),
            end_time=parser.parse(
                entry['productInfo']['productEndDate']
            ).replace(tzinfo=None),
        )

    def _get_license(self):
        if self._license is not None:
            return self._license

        r = requests.put(
            urljoin(self.config['url'],
                    'termsaccepted/Copernicus_General_License'),
            headers={
                'Content-Type': 'application/json',
                'Accept': 'text/html',
                'authorization': '{}'.format(self._get_token())}
        )
        if r.status_code != requests.codes.ok:
            raise ConnectionError('termsaccepted request failed')

        status = False
        while not status:
            r = requests.get(
                urljoin(self.config['url'],
                        'termsaccepted/Copernicus_General_License'),
                headers={
                    'Content-Type': 'application/json',
                    'authorization': '{}'.format(self._get_token())}
            )
            if r.status_code != requests.codes.ok:
                raise ConnectionError('termsaccepted request failed')

            status = r.json()['accepted']
            if not status:
                time.sleep(1)

        self._license = status

        return self._license

    def download(self, granules, mystore='.', **kwargs):
        if self._get_license() is None:
            self._get_license()

        if isinstance(granules, Granule):
            granules = [granules]

        for granule in granules:
            data = {
                "jobId": self.job,
                "uri": granule.url
            }
            r = requests.post(
                urljoin(self.config['url'], 'dataorder'),
                data=json.dumps(data),
                headers={'authorization': self._get_token(),
                         'content-type': 'application/json'},
            )
            if r.status_code != requests.codes.ok:
                raise ConnectionError('dataorder request failed')

            # order is in queue
            order = r.json()['orderId']
            while r.json()['status'] == 'running':
                r = requests.get(
                    urljoin(self.config['url'],
                            'dataorder/status/{}'.format(order)),
                    headers={'authorization': self._get_token()}
                )

            # get result
            outpath = self.get_local_storage_path(granule, mystore)
            if not outpath.exists():
                outpath.parent.mkdir(parents=True, exist_ok=True)

            g = copy.copy(granule)
            g.url = urljoin(
                        self.config['url'],
                        'dataorder/download/{}'.format(order)
                    )

            return super(Wekeo, self).download(
                g,
                mystore=mystore,
                **kwargs)

    def catalogue(
            self,
            fetch_all=True,
            start_page=0,
            items_per_page=50,
            **kwargs):
        raise NotImplementedError
