import logging
from urllib.parse import urljoin, quote

from dateutil import parser
import requests
import shapely.geometry

from eo_harvest.queries.granule import Granule
from eo_harvest.stores.opensearch import Opensearch


class Ifremer(Opensearch):
    store = 'Ifremer'
    max_items_per_page = 100

    def __init__(self, **kwargs):
        super(Ifremer, self).__init__(**kwargs)

    def _search(
            self,
            dataset_id, start_time, end_time, area,
            fetch_all: bool = True,
            start_page: int = 0,
            items_per_page: int = 400,
            silent=True,
            **kwargs
            ):
        params = 'startPage={}'.format(start_page)
        params += '&count={}'.format(items_per_page)
        params += '&timeStart={}'.format(
            start_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        params += '&timeEnd={}'.format(
            end_time.strftime('%Y-%m-%dT%H:%M:%SZ'))
        params += '&geoBox={},{},{},{}'.format(
            *[round(_, 3) for _ in area.bounds])
        for k, v in kwargs.items():
            params += '&{}={}'.format(k, v)

        url = urljoin(
                self.config['url'],
                'granules.atom?datasetId={}&{}'.format(dataset_id, params)
        )
        logging.info('fetching {}'.format(quote(url, safe='/:?&=')))
        if not silent:
            print('fetching {}'.format(quote(url, safe='/:?&=')))
        r = requests.get(url)

        if r.status_code != requests.codes.ok:
            print(r.text)
            raise ConnectionError('request failed')

        return r.text

    def _decode_granule(self, entry, dataset_id, **kwargs):
        date = entry.find("dc:date").text
        if '/' in date:
            start, end = date.split('/')
        else:
            start = end = date
        downloads = entry.find_all("link")
        download = {
            _.attrs['title']:_.attrs['href'] for _ in downloads
        }
        if entry.find("georss:point"):
            footprint = entry.find("georss:point").text
            footprint = shapely.geometry.Point(
                *[float(_) for _ in footprint.split()]
            )
        elif entry.find("georss:line"):
            footprint = entry.find("georss:line").text
            coords = [float(_) for _ in footprint.split()]
            coords = [(_, coords[i * 2 + 1]) for i, _ in enumerate(coords[::2])]
            footprint = shapely.geometry.LineString(coords)
        elif entry.find("georss:polygon"):
            footprint = entry.find("georss:polygon").text
            coords = [float(_) for _ in footprint.split()]
            coords = [(_, coords[i * 2 + 1]) for i, _ in enumerate(coords[::2])]
            footprint = shapely.geometry.Polygon(coords)
        elif entry.find("georss:where"):
            if entry.find("gml:MultiPolygon") is not None:
                polycoords = entry.find(
                    "gml:MultiPolygon").find_all('gml:coordinates')
                polygons = []
                for coord in polycoords:
                    coords = [(float(lon), float(lat))
                              for lon, lat in
                              [c.split(',') for c in coord.text.split(' ')]]
                    polygons.append(shapely.geometry.Polygon(coords))
                footprint = shapely.geometry.MultiPolygon(polygons)
            elif entry.find("gml:MultiCurve") is not None:
                polylines = []
                for member in entry.find_all("gml:LineString"):
                    polycoords = member.find_all('gml:posList')
                    for coord in polycoords:
                        coords = [float(_) for _ in coord.text.split()]
                        coords = [(_, coords[i * 2 + 1]) for i, _ in
                                  enumerate(coords[::2])]
                        polylines.append(shapely.geometry.LineString(coords))
                footprint = shapely.geometry.MultiLineString(polylines)
            else:
                raise ValueError("Unknown result geometry")
        else:
            #print(entry.text)
            raise ValueError("Unknown result geometry")

        return Granule(
            name=entry.title.text,
            url=download,
            store=self,
            dataset_id=dataset_id,
            start_time=parser.parse(start).replace(tzinfo=None),
            end_time=parser.parse(end).replace(tzinfo=None),
            shape=footprint
        )
