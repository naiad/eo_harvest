from dateutil import parser

from bs4 import BeautifulSoup
import shapely.geometry

from podaac import podaac as podaac
from podaac import podaac_utils as utils
from podaac import drive as drive

from .store import Store
from eo_harvest.queries.dataset import Dataset
from eo_harvest.queries.response import Response
from eo_harvest.queries.granule import Granule


MAX_TRIES = 3


class PODAACDrive(Store):
    store = 'PODAACDrive'

    def __init__(self, **kwargs):
        super(PODAACDrive, self).__init__(**kwargs)
        self._podaac = podaac.Podaac()
        #u = utils.PodaacUtils()
        self._drive = drive.Drive(
            None, self.config['username'], self.config['password'],
            self.config['url']
        )

    def _decode_granule(self, entry, dataset_id, **kwargs):
        opendap_url = entry.find('link', title='OPeNDAP URL').attrs['href']

        return Granule(
            name=entry.title.text,
            url=entry.find('link', title='HTTP URL').attrs['href'],
            store=self,
            dataset_id=entry.datasetId.text,
            boundaries=shapely.geometry.box(
                *(map(
                    [float(_) for _ in entry.box.text.split()].__getitem__,
                    [1, 0, 3, 2]))),
            start_time=parser.parse(entry.start.text).replace(tzinfo=None),
            end_time=parser.parse(entry.end.text).replace(tzinfo=None),
            opendap_url=opendap_url
        )

    def search_granule(
            self,
            dataset, start_time, end_time, area,
            fetch_all=True,
            start_page=0,
            items_per_page=400,
            silent=True,
            **kwargs):

        complete = False
        granules = []
        while not complete:
            # requests to PO.DAAC drive sometimes fail. Manage here retries
            # if needed.
            nb_tries = 0
            while nb_tries < MAX_TRIES:
                try:
                    resp = self._podaac.granule_search(
                        dataset_id=dataset,
                        start_time=start_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
                        end_time=end_time.strftime('%Y-%m-%dT%H:%M:%SZ'),
                        bbox='{},{},{},{}'.format(*area.envelope.bounds),
                        sort_by='timeAsc',
                        start_index=str(start_page),
                        items_per_page=str(items_per_page),
                        _format='atom'
                    )
                    # success: get out
                    break
                except:
                    nb_tries += 1
                    if nb_tries == MAX_TRIES:
                        raise
                    print("Request failed. Retrying.")

            soup = BeautifulSoup(resp, 'xml')
            if soup.find('totalResults') is None:
                return Response(granules, 0, 0, 0)

            rtotal = int(soup.find('totalResults').getText())
            rstart = int(soup.find('startIndex').getText())
            rcount = int(soup.find('itemsPerPage').getText())
            #print(soup.prettify())

            granules.extend(
                [self._decode_granule(_) for _ in soup.find_all('entry')]
            )
            response = Response(granules, rtotal, rstart, rcount)
            if fetch_all and not response.complete():
                start_page = response.next()
            else:
                complete = True

        return response

    def get_granule(self, granules, mystore='.', **kwargs):
        self._drive.download_granules(
            granule_collection=granules, path=mystore)

    def catalogue(
            self,
            fetch_all=True,
            start_index=0,
            items_per_page=50,
            **kwargs):

        complete = False
        datasets = []
        while not complete:
            resp = self._podaac.dataset_search(
                start_index=str(start_index),
                items_per_page=str(items_per_page),
                _format='atom', full='False'
            )

            soup = BeautifulSoup(resp, 'xml')
            rtotal = int(soup.find('totalResults').getText())
            rstart = int(soup.find('startIndex').getText())
            rcount = int(soup.find('itemsPerPage').getText())

            for dataset in soup.find_all('entry'):
                datasets.append(
                    Dataset(dataset.id.string, dataset.title.string)
                )

            response = Response(datasets, rtotal, rstart, rcount)
            if fetch_all and not response.complete():
                start_index = response.next()
            else:
                complete = True

        return response

