import logging

from dateutil import parser
import requests
from urllib.parse import urljoin, quote

import shapely

from eo_harvest.queries.granule import Granule
from eo_harvest.stores.opensearch import Opensearch


class PEPS(Opensearch):
    store = 'PEPS'

    def __init__(self, **kwargs):
        super(PEPS, self).__init__(**kwargs)

    @classmethod
    def _decode_gml(cls, entry):
        """decode georss/gml polygon"""
        polygons = []
        for polygon in entry.find_all('Polygon'):
            for coord in polygon.find('posList'):
                coords = [float(_) for _ in coord.split()]
                coords = [(coords[i*2+1], _) for i, _ in enumerate(coords[::2])]
                polygons.append(shapely.geometry.Polygon(coords))

        if len(polygons) == 1:
            return polygons[0]

        return shapely.geometry.MultiPolygon(polygons)

    def _decode_granule(self, entry, dataset_id, **kwargs):
        footprint = self._decode_gml(entry.where)
        return Granule(
            name=entry.title.text,
            url=entry.find('link', rel='enclosure').attrs['href'],
            store=self,
            dataset_id=dataset_id,
            shape=footprint,
            boundaries=footprint.envelope,
            start_time=parser.parse(entry.beginPosition.text),
            end_time=parser.parse(entry.endPosition.text),
            quicklook=entry.find('link', rel='icon').attrs['href']
        )

    def _search(
            self,
            dataset_id, start_time, end_time, area,
            fetch_all: bool = True,
            start_page: int = 0,
            items_per_page: int = 400,
            silent=True,
            **kwargs
            ):
        params = 'maxRecords={}'.format(items_per_page)
        params += '&startDate={}'.format(start_time.isoformat())
        params += '&completionDate={}'.format(end_time.isoformat())
        params += '&geometry={}'.format(
            shapely.geometry.polygon.orient(area.simplify(0.2)).wkt)
        for k, v in kwargs.items():
            params += '&{}={}'.format(k, v)

        url = urljoin(
                self.config['url'],
                'api/collections/{}/search.atom?{}'.format(dataset_id, params)
        )
        logging.info('fetching {}'.format(quote(url, safe='/:?&=')))
        if not silent:
            print('fetching {}'.format(quote(url, safe='/:?&=')))
        r = requests.get(url)

        if r.status_code != requests.codes.ok:
            print(r.text)
            raise ConnectionError('request failed')

        return r.text