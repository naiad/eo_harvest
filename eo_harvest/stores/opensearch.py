import requests
from urllib.parse import urljoin

from bs4 import BeautifulSoup

from .store import Store
from eo_harvest.queries.dataset import Dataset
from eo_harvest.queries.granule import Granule
from eo_harvest.queries.response import Response


class Opensearch(Store):
    store = 'Opensearch'

    def __init__(self, **kwargs):
        super(Opensearch, self).__init__(**kwargs)

    def _search(self,
                dataset_id, start_time, end_time, area,
                fetch_all: bool = True,
                start_page: int = 0,
                items_per_page: int = 400,
                **kwargs
                ):
        raise NotImplementedError

    def _decode_granule(self, entry, dataset_id, **kwargs):
        import re
        download = entry.find(
            href=re.compile("download"))
        quicklook = entry.find(
            href=re.compile("quicklook"))

        return Granule(
            name=entry.title.text,
            dataset_id=dataset_id,
            url=download.attrs['href'],
            quicklook=quicklook.attrs['href']
        )

    def decode_search_result(self, resp):
        soup = BeautifulSoup(resp, 'xml')
        entries = soup.find_all('entry')
        rtotal = int(soup.find('totalResults').getText())
        rstart = int(soup.find('startIndex').getText())
        rcount = int(soup.find('itemsPerPage').getText())

        return entries, rtotal, rstart, rcount,

    def get_granule(self, granules, mystore='.', **kwargs):
        self._drive.download_granules(
            granule_collection=granules, path=mystore)

    def catalogue(
            self,
            fetch_all=True,
            start_page=0,
            items_per_page=50,
            **kwargs):

        complete = False
        datasets = []
        while not complete:
            resp = self._podaac.dataset_search(
                start_page=str(start_page),
                items_per_page=str(items_per_page),
                _format='atom', full='False'
            )

            soup = BeautifulSoup(resp, 'xml')
            rtotal = int(soup.find('totalResults').getText())
            rstart = int(soup.find('startIndex').getText())
            rcount = int(soup.find('itemsPerPage').getText())

            for dataset in soup.find_all('entry'):
                datasets.append(
                    Dataset(dataset.id.string, dataset.title.string)
                )

            response = Response(datasets, rtotal, rstart, rcount)
            if fetch_all and not response.complete():
                start_page = response.next()
            else:
                complete = True

        return response

