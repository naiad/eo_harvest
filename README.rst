==========
eo_harvest
==========

Overview
========

``eo-harvest`` is a python package that allows to perform data searches from
different online Earth Obervation (EO) data providers, with a
unified API and simple spatial and temporal criteria.

Each data access service is referred to a class of **data store**, implementing
a particular type of search API. Currently managed data stores include:

* CODA: data store for Copernicus Sentinel-3 products
  (https://coda.eumetsat.int, https://codarep.eumetsat.int)
* Wekeo: data stores for various ECMWF, CMEMS and Eumetsat EO products
  (https://wekeo.eu)
* PEPS: CNES managed data store for Sentinel-1, Sentinel-2 and Sentinel-3
  products (https://peps.cnes.fr)
* PODAAC: data store for US JPL/PO.DAAC data center
  (https://podaac-tools.jpl.nasa.gov/drive)


The API allows to perform the following operations:
  * catalogue
  * search
  * multi-search
  * quicklook
  * download

Note that these operations are not all available at each data store, depending
on the data store offered services. When an operation is not available for a
data store, an exception is thrown.

Installation
============

The package can be downloaded at: https://gitlab.ifremer.fr/naiad/eo_harvest

Install with ``pip`` in a conda or virtual env.

External dependencies (automatically installed with pip):
  * ``lxml``
  * ``bs4``
  * ``podaacpy``
  * ``cartopy``


Configuration
=============

To use the API, you need to create a configuration file, that must be saved in
your home dir as: ``$HOME/.naiad/stores.yaml``. This configuration file is in
YAML format.

Many (if not all) stores require some personal authentication: the URLs of the
store APIs and your credential must be save in the configuration file.

The configuration file has a specific section for each store you wish to connect
to. Usually each data access service has its own unique API (PODAACDrive, PEPS,
Wekeo,...) and this API can then only be used with one single corresponding
service URL. The section name can then simply be the API class name. However
some API are more generic, and shared by different online services: to resolve
this ambiguity, the section name must be a unique identifier (that you can
choose freely) and we must associate in the section details the corresponding
API class, in ``type`` field.

Here is an example of such configuration file. Just replace with your own
identifiers:

.. code-block:: yaml

  stores:

      PODAACDrive:
        username: <your login to PODAAC Drive>
        password: <your password>
        url: https://podaac-tools.jpl.nasa.gov/drive/files

      Wekeo:
        username: <your login to WekeO>
        password: <your password>
        url: https://wekeo-broker.apps.mercator.dpi.wekeo.eu/databroker/

      PEPS:
        url: https://peps.cnes.fr/resto/

      CODA:
        username: <your login to CODA>
        password: <your password>
        url: https://coda.eumetsat.int

      CODAREP:
        # here this another store using the same API as CODA. There is an ambiguity
        # between the service class (API) and the actual service to connect to. You
        # must explicitly set the service class in ``type``.
        type: CODA
        username: <your login to CODA>
        password: <your password>
        url: https://coda.eumetsat.int

      Ifremer:
        url: https://opensearch.ifremer.fr


Example
=======

Examples are provided as jupyter notebooks in the git repo, for each data store
class.
